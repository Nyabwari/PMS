import 'package:flutter/material.dart';
import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';

import 'Alerts.dart';
import 'InkWellDrawer.dart';
import 'dart:async';

import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cached_pdfview/flutter_cached_pdfview.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'dart:convert' as convert;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:geolocator/geolocator.dart';
import 'package:geocoder/geocoder.dart';
import 'package:intl/intl.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:url_launcher/url_launcher.dart';


import 'InkWellDrawer.dart';

const nyekundu = Color(0xffe32e16);
const tabcolor = Color(0xffe1dede);
const nyeupe = Color(0xffffffff);
const listcolor = Color(0xffffbfbfd);
const listcolorr = Color(0xfffebecf0);
const kijani = Colors.green;
const gray = Color(0xffa9a9a9);
const nyeusi= Color(0xff000000);
const mainColor = Color(0xff7f5496);

String cdst="Fetching Location... ";
String jina=" ";
String address="Loading address... ";
String id="";
String lat=" ";
String lon=" ";
String alt=" ";
String town=" ";
String online="";
String taskId="";
String akishoni=" ";
String street=" ";
String fon=" ";
String emair=" ";
String nem=" ";
String lastCheck=" ";
ProgressDialog pr, prr;
LatLng _center;
String messo=" ";
String pesa="ksh";
bool statee=false;

String refreshh="0";
String mesho="No updates found in the last four hours";
String  company;
String usertokeni = '';
String clogo="http://alerts.p-count.org/k.png";
String alogo="http://alerts.p-count.org/dirLogos/rock_logo.PNG";

GoogleMapController controller;
int hesabu=0;
String defaultMessage = "Enchogu";
Set<Marker> markers;
String pdfurl="";
String shtate="";
LatLng currentLocation =
LatLng(-1.286389, 36.817223);
Marker m;
FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
String etokeni=" ";
List<TaskList> literature;
List<TaskListt> inliterature;
FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
new FlutterLocalNotificationsPlugin();



class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}
Future<List<TaskListt>> _getUserss() async {
  print("uuuuzi");
  String apiUrl = "http://projects.qooetu.com/mobile/V2/UserDashboard";
  Map<String, String> headers = {"Content-type": "application/json", "token": usertokeni };
  final json =  convert.jsonEncode({  "token": usertokeni});
  http.Response response = await http.post(apiUrl,headers: headers, body: json);
  String jsonsDataString = response.body.toString();

  print("leo ni leo");
  print(jsonsDataString);
  print(usertokeni);
  if (response.statusCode == 200) {

    String jsonsDataString = response.body.toString();

    Map<String, dynamic> map = jsonDecode(jsonsDataString);
    SharedPreferences prefs = await SharedPreferences.getInstance();

    List<TaskListt> alerts = [];

    if (map['DashboardData'] != null) {
      print("sawasawa");
      inliterature = new List<TaskListt>();
      map['DashboardData'].forEach((v) {
        print("oroo1");


        TaskListt alert = TaskListt(v["ProjectID"].toString(),v["ProjectCode"].toString(),v["ProjectName"].toString(), v["ProjectProgress"].toString(), v["SiteAttachedTo"].toString(), v["NoofCasualWorkers"].toString(), v["TasksTagged"].toString(), v["ProjectStatus"].toString());

        alerts.add(alert);
        print("200 bob imepotea");
      });
      print(alerts.length);

      return alerts;

    }


  }
  else {
    print("no");
  }

}
class _DashboardState extends State<Dashboard> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: InkWellDrawer(),
      backgroundColor: mainColor,

      appBar: AppBar(

        iconTheme: IconThemeData(
            color: Colors.white
        ),
        actions: <Widget>[
          PopupMenuButton<String>(
            itemBuilder: (BuildContext context) {
              return {online}.map((String choice) {
                return PopupMenuItem<String>(
                  value: choice,
                  child: Text(choice),
                );
              }).toList();
            },
          ),

        ],
        centerTitle: true,
        backgroundColor: mainColor,
        title: Text(
          " Projects" +" Projects",
          style: TextStyle(
            color: Colors.white,
            fontSize: 17,

          ),
        ),
      ),
      body: CustomScrollView(
        physics: ClampingScrollPhysics(),
        slivers: [

          _buildRegionTabBar(),
          SliverPadding(
            padding: const EdgeInsets.symmetric(horizontal: 5.0),
            sliver: SliverToBoxAdapter(
              child: StatsGrid(),
            ),
          ),
          SliverPadding(
            padding: const EdgeInsets.symmetric(horizontal: 5.0),
            sliver: SliverToBoxAdapter(
              child:     Container(
                height: 400.0,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20.0),
                    topRight: Radius.circular(20.0),
                  ),
                ),
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        padding: const EdgeInsets.all(20.0),
                        child: Text(
                          'Detailed Projects Overview',
                          style: const TextStyle(
                            fontSize: 16.0,
                            decoration: TextDecoration.underline,

                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.95,

                        color: mainColor,
                        child: FutureBuilder(
                          future: _getUserss(),
                          builder: (BuildContext context, AsyncSnapshot snapshot){
                            print(snapshot.data);
                            if(snapshot.data == null){
                              return Container(
                                  width: MediaQuery.of(context).size.width * 0.95,

                                  color:listcolorr,
                                  child: Center(
                                      child: CircularProgressIndicator()
                                  )
                              );
                            } else {
                              return
                                Container(
                                  height: MediaQuery.of(context).size.height * 0.85,

                                  color:listcolorr,
                                  child: ListView.builder(
                                    padding: EdgeInsets.only(bottom:300, top:5),
                                    itemCount: snapshot.data.length,
                                    itemBuilder: (BuildContext context, int index) {
                                      return Container(
                                        color: mainColor,
                                        child: Card(
                                          color: tabcolor,
                                          margin: EdgeInsets.all(0.5),
                                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                                          elevation: 4.0,
                                          child: Container(
                                            padding: EdgeInsets.only(right:12.5),

                                            decoration: BoxDecoration(color: listcolorr,
                                              borderRadius: BorderRadius.circular(10),
                                            ),
                                            child: ExpansionTile(
                                                title: Container(
                                                  width: double.infinity,


                                                  child: Column(
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    children: <Widget>[
                                                      Container(height: 3, color: Colors.transparent),
                                                      Text("Task: "+snapshot.data[index].TaskId+"/"+snapshot.data[index].TaskNumber,style: TextStyle( fontSize: 18, color:nyeusi,
                                                          fontFamily: 'Montserrat-Regular',
                                                          decoration: TextDecoration.underline,
                                                          fontWeight: FontWeight.bold),),
                                                      Container(height: 5, color: Colors.transparent),
                                                      Text("Description: "+snapshot.data[index].TaskTitle,style: TextStyle(fontSize: 16),),
                                                      Container(height: 5, color: Colors.transparent),

                                                      Text.rich(
                                                        TextSpan(
                                                          children: [
                                                            TextSpan(text: "Task Status: ",
                                                              style: TextStyle(
                                                                fontSize: 16,
                                                                color:nyeusi,
                                                                fontFamily: 'Montserrat-Regular',


                                                              ),),
                                                            TextSpan(
                                                              text: snapshot.data[index].Status,

                                                              style: TextStyle(
                                                                fontSize: 16,
                                                                color:nyeusi,
                                                                fontFamily: 'Montserrat-Regular',),
                                                            ),
                                                          ],
                                                        ),
                                                      ),

                                                      Container(height: 8, color: Colors.transparent),
                                                    ],
                                                  ),
                                                ),
                                                onExpansionChanged: (value){
                                                  setState(() {
                                                  });
                                                },
                                                children: [
                                                  Container(
                                                    padding: EdgeInsets.only(right: 15.0, top: 5, left:15.0),

                                                    child: Column(
                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                      children: <Widget>[

                                                        Text.rich(
                                                          TextSpan(
                                                            children: [
                                                              TextSpan(text: "Project Code: ",
                                                                style: TextStyle(
                                                                  fontSize: 16,
                                                                  color:nyeusi,
                                                                  fontFamily: 'Montserrat-Regular',


                                                                ),),
                                                              TextSpan(
                                                                text: snapshot.data[index].ProjectCode,

                                                                style: TextStyle(
                                                                  fontSize: 16,
                                                                  color:nyeusi,
                                                                  fontFamily: 'Montserrat-Regular',),
                                                              ),
                                                            ],
                                                          ),
                                                        ),

                                                        Container(height: 8, color: Colors.transparent),
                                                        Text.rich(
                                                          TextSpan(
                                                            children: [
                                                              TextSpan(text: "Project Name: ",
                                                                style: TextStyle(
                                                                  fontSize: 16,
                                                                  color:nyeusi,
                                                                  fontFamily: 'Montserrat-Regular',


                                                                ),),
                                                              TextSpan(
                                                                text: snapshot.data[index].ProjectName,

                                                                style: TextStyle(
                                                                  fontSize: 16,
                                                                  color:nyeusi,
                                                                  fontFamily: 'Montserrat-Regular',),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                        Container(height: 8, color: Colors.transparent),
                                                        Text.rich(
                                                          TextSpan(
                                                            children: [
                                                              TextSpan(text: "Milestone Name: ",
                                                                style: TextStyle(
                                                                  fontSize: 16,
                                                                  color:nyeusi,
                                                                  fontFamily: 'Montserrat-Regular',


                                                                ),),
                                                              TextSpan(
                                                                text: snapshot.data[index].MileStoneName,

                                                                style: TextStyle(
                                                                  fontSize: 16,
                                                                  color:nyeusi,
                                                                  fontFamily: 'Montserrat-Regular',),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                        Container(height: 8, color: Colors.transparent),



                                                      ],
                                                    ),
                                                  ),
                                                ]
                                            ),
                                          ),
                                        ),
                                      );
                                    },
                                  ),
                                );
                            }
                          },
                        ),


                      ),
                      Container(

                      )
                    ],
                  ),
                ),
              )
            ),
          ),


        ],
      ),
    );
  }


  SliverToBoxAdapter _buildRegionTabBar() {
    return SliverToBoxAdapter(
      child: DefaultTabController(
        length: 2,
        child: Container(
          margin: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 10),
          height: 50.0,
          decoration: BoxDecoration(
            color: Colors.white24,
            borderRadius: BorderRadius.circular(25.0),
          ),
          child: TabBar(
            indicator: BubbleTabIndicator(
              tabBarIndicatorSize: TabBarIndicatorSize.tab,
              indicatorHeight: 40.0,
              indicatorColor: Colors.white,
            ),

            labelColor: Colors.black,
            unselectedLabelColor: Colors.white,
            tabs: [
              Text('Ongoing Projects (17)'),
              Text('Completed Projects (22)'),
            ],
            onTap: (index) {},
          ),
        ),
      ),
    );
  }
}

class StatsGrid extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.45,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center ,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [

          Flexible(
            child: Row(
      mainAxisAlignment: MainAxisAlignment.center ,
      crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                _buildStasCard('Active Projects', '11 Projects', Colors.orange),
                _buildStasCard('Paused Projects', '6 Projects', Colors.orange),
              ],
            ),
          ),
          Flexible(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center ,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                _buildStasCard('Active', '20 Tasks', Colors.green),
                _buildStasCard('Paused', '8 Tasks', Colors.green),
                _buildStasCard('Completed', '16 Tasks', Colors.green),
              ],
            ),
          ),
          Flexible(

            child: Row(
              mainAxisAlignment: MainAxisAlignment.center ,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                _buildStasCard('Enrolled Casuals', '60 Casuals', Colors.blue),
                _buildStasCard('Active Casuals', '48 Tasks', Colors.blue),
              ],
            ),
          ),
          Flexible(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center ,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                _buildStasCard('Incidences Reported', '12 Incidences', Colors.pink),
              ],
            ),
          ),

        ],
      ),
    );
  }

  Expanded _buildStasCard(String title, String count, MaterialColor color) {
    return Expanded(
      child: Container(
        margin: const EdgeInsets.all(6.0),
        padding: const EdgeInsets.all(10.0),
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              title,
              style: const TextStyle(
                color: Colors.white,
                fontSize: 15.0,
                fontWeight: FontWeight.w600,
              ),
            ),
            Text(
              count,
              style: const TextStyle(
                color: Colors.white,
                fontSize: 18.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

  class PDFViewerFromUrl extends StatelessWidget {
  const PDFViewerFromUrl({Key key, @required this.url}) : super(key: key);

  final String url;

  @override
  Widget build(BuildContext context) {
  return Scaffold(
  appBar: AppBar(
  title: Text(pdfurl),
  ),
  body: const PDF().fromUrl(
  url,
  placeholder: (double progress) => Center(child: Text('$progress %')),
  errorWidget: (dynamic error) => Center(child: Text(error.toString())),
  ),
  );
  }
  }


  class TaskList {
  String TaskId;
  String ProjectCode;
  String ProjectName;
  String MileStoneName;
  String TaskNumber;
  String TaskTitle;
  String Priority;
  String Status;




  TaskList(this.TaskId, this.ProjectCode, this.ProjectName, this.MileStoneName,this.TaskNumber, this.TaskTitle, this.Priority, this.Status);

  }
  class TaskListt {
  String TaskId;
  String ProjectCode;
  String ProjectName;
  String MileStoneName;
  String TaskNumber;
  String TaskTitle;
  String Priority;
  String Status;


  TaskListt(this.TaskId, this.ProjectCode, this.ProjectName, this.MileStoneName,this.TaskNumber, this.TaskTitle, this.Priority, this.Status);
  }

  class Holidays {
  String HolidayId;
  String HolidayName;
  String HolidayDescription;
  String HolidayStartDateTime;
  String HolidayEndDateTime;

  Holidays(this.HolidayId, this.HolidayName, this.HolidayDescription, this.HolidayStartDateTime,  this.HolidayEndDateTime);
  }