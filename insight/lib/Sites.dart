import 'dart:async';

import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cached_pdfview/flutter_cached_pdfview.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'dart:convert' as convert;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:geolocator/geolocator.dart';
import 'package:geocoder/geocoder.dart';
import 'package:intl/intl.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:url_launcher/url_launcher.dart';


import 'InkWellDrawer.dart';

const mainColor = Color(0xfff089225);
const nyekundu = Color(0xffe32e16);
const tabcolor = Color(0xffe1dede);
const nyeupe = Color(0xffffffff);
const listcolor = Color(0xffffbfbfd);
const listcolorr = Color(0xfffebecf0);
const kijani = Colors.green;
const gray = Color(0xffa9a9a9);
const nyeusi= Color(0xff000000);

String cdst="Fetching Location... ";
String jina=" ";
String address="Loading address... ";
String id="";
String lat=" ";
String lon=" ";
String alt=" ";
String town=" ";
String online="";
String taskId="";
String akishoni=" ";
String street=" ";
String fon=" ";
String emair=" ";
String nem=" ";
String lastCheck=" ";
ProgressDialog pr, prr;
LatLng _center;
String messo=" ";
String pesa="ksh";
bool statee=false;

String refreshh="0";
String mesho="No updates found in the last four hours";
String  company;
String usertokeni = '';
String clogo="http://alerts.p-count.org/k.png";
String alogo="http://alerts.p-count.org/dirLogos/rock_logo.PNG";

GoogleMapController controller;
int hesabu=0;
String defaultMessage = "Enchogu";
Set<Marker> markers;
String pdfurl="";
String shtate="";
LatLng currentLocation =
LatLng(-1.286389, 36.817223);
Marker m;
FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
String etokeni=" ";
List<TaskList> literature;
List<TaskListt> inliterature;
FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
new FlutterLocalNotificationsPlugin();

class Sites extends StatefulWidget {

  _Sites createState() => _Sites();

}


class  _Sites extends State< Sites> {



  @override
  void initState() {
    super.initState();
    _restore();
    _determinePosition();
    const oneSec = const Duration(seconds:60);
    new Timer.periodic(oneSec, (Timer t) => setState(() {
      refreshh;
    }));
  }


  _restore() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    usertokeni = prefs.getString('token');
    print("choyy");
    setState(() {
      setState(() {
        usertokeni;
      });
    });
  }
  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    StreamSubscription<Position> positionStream = Geolocator.getPositionStream(desiredAccuracy: LocationAccuracy.best, timeInterval: 30000).listen(

            (position) async {
          print(DateTime.now());
          final coordinates = new Coordinates(position.latitude, position.longitude);
          setState(()
          {
            position;
            print("Sasa Antho");

            cdst= "("+position.latitude.toString() +", "+position.longitude.toString()+")"+position.altitude.toStringAsFixed(4);
            print(cdst);
            lat=position.latitude.toString();
            lon=position.longitude.toString();

          });
          setState(() async {
            var addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);
            var first = addresses.first;
            address=first.locality+", "+first.countryName;
            town=first.locality;
            street=first.addressLine;
          });

        });

    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.deniedForever) {
      return Future.error(
          'Location permissions are permantly denied, we cannot request permissions.');
    }

    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission != LocationPermission.whileInUse &&
          permission != LocationPermission.always) {
        return Future.error(
            'Location permissions are denied (actual value: $permission).');
      }
    }

    return await Geolocator.getCurrentPosition();
  }
  Future<void> _run() async {

    pr.show();
    print("sasa");
    String apiUrl = "http://projects.qooetu.com/mobile/V2/StartTask";
    Map<String, String> headers = {"Content-type": "application/json","token": usertokeni };
    final json =  convert.jsonEncode({"TaskId": taskId});
    http.Response response = await http.post(apiUrl,headers: headers, body: json);
    String jsonsDataString = response.body.toString();

    print("Endmonth");

    print(jsonsDataString);
    if (response.statusCode == 200) {
      pr.hide().then((isHidden) {
        print(isHidden);
      });

      String jsonsDataString = response.body.toString();

      Map<String, dynamic> map = jsonDecode(jsonsDataString);

      print("Endmonth");

      print(jsonsDataString);
      pr.hide().then((isHidden) {
        print(isHidden);
      });

      setState(() { _getUsers(); });
      Fluttertoast.showToast(
          msg: map['Message'].toString(),
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 3,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0
      );

    }
    else {
      pr.hide().then((isHidden) {
        print(isHidden);
      });
      print("no");
    }
  }
  Future<void> _resume() async {

    pr.show();
    print("sasa");
    String apiUrl = "http://projects.qooetu.com/mobile/V2/ResumeTask";
    Map<String, String> headers = {"Content-type": "application/json","token": usertokeni };
    final json =  convert.jsonEncode({"TaskId": taskId});
    http.Response response = await http.post(apiUrl,headers: headers, body: json);
    String jsonsDataString = response.body.toString();

    print("Endmonth");

    print(jsonsDataString);
    if (response.statusCode == 200) {
      pr.hide().then((isHidden) {
        print(isHidden);
      });

      String jsonsDataString = response.body.toString();

      Map<String, dynamic> map = jsonDecode(jsonsDataString);

      print("Endmonth");

      print(jsonsDataString);
      pr.hide().then((isHidden) {
        print(isHidden);
      });

      setState(() { _getUsers(); });
      Fluttertoast.showToast(
          msg: map['Message'].toString(),
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 3,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0
      );

    }
    else {
      pr.hide().then((isHidden) {
        print(isHidden);
      });
      print("no");
    }
  }
  Future<void> _pause() async {

    pr.show();
    print("sasa");
    String apiUrl = "http://projects.qooetu.com/mobile/V2/PauseTask";
    Map<String, String> headers = {"Content-type": "application/json","token": usertokeni };
    final json =  convert.jsonEncode({"TaskId": taskId});
    http.Response response = await http.post(apiUrl,headers: headers, body: json);
    String jsonsDataString = response.body.toString();

    print("Endmonth");

    print(jsonsDataString);
    if (response.statusCode == 200) {
      pr.hide().then((isHidden) {
        print(isHidden);
      });

      String jsonsDataString = response.body.toString();

      Map<String, dynamic> map = jsonDecode(jsonsDataString);

      print("Endmonth");

      print(jsonsDataString);
      pr.hide().then((isHidden) {
        print(isHidden);
      });

      setState(() { _getUsers(); });
      Fluttertoast.showToast(
          msg: map['Message'].toString(),
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 3,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0
      );

    }
    else {
      pr.hide().then((isHidden) {
        print(isHidden);
      });
      print("no");
    }
  }
  Future<void> _end() async {

    pr.show();
    print("sasa");
    String apiUrl = "http://projects.qooetu.com/mobile/V2/FinishTask";
    Map<String, String> headers = {"Content-type": "application/json","token": usertokeni };
    final json =  convert.jsonEncode({"TaskId": taskId});
    http.Response response = await http.post(apiUrl,headers: headers, body: json);
    String jsonsDataString = response.body.toString();

    print("Endmonth");

    print(jsonsDataString);
    if (response.statusCode == 200) {
      pr.hide().then((isHidden) {
        print(isHidden);
      });

      String jsonsDataString = response.body.toString();

      Map<String, dynamic> map = jsonDecode(jsonsDataString);

      print("Endmonth");

      print(jsonsDataString);
      pr.hide().then((isHidden) {
        print(isHidden);
      });

      setState(() { _getUsers(); });
      Fluttertoast.showToast(
          msg: map['Message'].toString(),
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 3,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0
      );

    }
    else {
      pr.hide().then((isHidden) {
        print(isHidden);
      });
      print("no");
    }
  }



  Future<List<TaskList>> _getUsers() async {
    print("uuuuzi");
    String apiUrl = "http://projects.qooetu.com/mobile/V2/UserSites";
    Map<String, String> headers = {"Content-type": "application/json", "token": usertokeni };
    final json =  convert.jsonEncode({  "token": usertokeni});
    http.Response response = await http.post(apiUrl,headers: headers, body: json);
    String jsonsDataString = response.body.toString();

    print(jsonsDataString);
    print(usertokeni);
    if (response.statusCode == 200) {

      String jsonsDataString = response.body.toString();

      Map<String, dynamic> map = jsonDecode(jsonsDataString);
      SharedPreferences prefs = await SharedPreferences.getInstance();

      List<TaskList> alerts = [];

      if (map['SitesData'] != null) {
        print("sawasawa");
        literature = new List<TaskList>();
        map['SitesData'].forEach((v) {
          print("oroo1");


          TaskList alert = TaskList(v["ProjectID"].toString(),v["ProjectCode"].toString(),v["ProjectName"].toString(), v["ProjectProgress"].toString(), v["SiteAttachedTo"].toString(), v["NoofCasualWorkers"].toString(), v["TasksTagged"].toString(), v["ProjectStatus"].toString());

          alerts.add(alert);
          print("200 bob imepotea");
        });
        print(alerts.length);

        return alerts;

      }


    }
    else {
      print("no");
    }

  }


  Future<List<TaskListt>> _getUserss() async {
    print("uuuuzi");
    String apiUrl = "http://projects.qooetu.com/mobile/V2/UserSites";
    Map<String, String> headers = {"Content-type": "application/json", "token": usertokeni };
    final json =  convert.jsonEncode({  "token": usertokeni});
    http.Response response = await http.post(apiUrl,headers: headers, body: json);
    String jsonsDataString = response.body.toString();

    print(jsonsDataString);
    print(usertokeni);
    if (response.statusCode == 200) {

      String jsonsDataString = response.body.toString();

      Map<String, dynamic> map = jsonDecode(jsonsDataString);
      SharedPreferences prefs = await SharedPreferences.getInstance();

      List<TaskListt> alerts = [];

      if (map['SitesData'] != null) {
        print("sawasawa");
        inliterature = new List<TaskListt>();
        map['SitesData'].forEach((v) {
          print("oroo1");


          TaskListt alert = TaskListt(v["ProjectID"].toString(),v["ProjectCode"].toString(),v["ProjectName"].toString(), v["ProjectProgress"].toString(), v["SiteAttachedTo"].toString(), v["NoofCasualWorkers"].toString(), v["TasksTagged"].toString(), v["ProjectStatus"].toString());

          alerts.add(alert);
          print("200 bob imepotea");
        });
        print(alerts.length);

        return alerts;

      }


    }
    else {
      print("no");
    }

  }


  ListView _jobsListView(literature) {
    return ListView.builder(
      padding: EdgeInsets.only(bottom:120),
      shrinkWrap: true,
      scrollDirection: Axis.vertical,
      itemCount: hesabu,
      itemBuilder: (BuildContext context, int index) {
        return userList(context, index);
      },
    );
  }
  Widget userList(BuildContext context, int index) {

  }
  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context,type: ProgressDialogType.Normal, isDismissible: true, showLogs: true);
    pr.style(
        message: 'Reporting...',
        borderRadius: 10.0,
        backgroundColor: Colors.grey.withOpacity(0.8),
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600)
    );
    DateTime now = DateTime.now();
    String formattedDate = DateFormat('hh:mm a').format(now);
    setState(() {
      formattedDate;
    });

    return Scaffold(
      drawer: InkWellDrawer(),
      backgroundColor: listcolor,

      appBar: AppBar(

        iconTheme: IconThemeData(
            color: Colors.black
        ),
        actions: <Widget>[
          PopupMenuButton<String>(
            itemBuilder: (BuildContext context)  {
              return {online}.map((String choice) {
                return PopupMenuItem<String>(
                  value: choice,
                  child: Text(choice),
                );
              }).toList();
            },
          ),

        ],
        centerTitle: true,
        backgroundColor: listcolorr,
        title: Text(
          "Sites Dashboard",
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.bold,

          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Stack(

          children: <Widget>[

            Column(
              crossAxisAlignment: CrossAxisAlignment.center,

              children: <Widget>[


                DefaultTabController(
                  length: 1,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Container(
                        color: gray,

                        child: Material(
                          color: gray,
                          child: TabBar(
                            indicatorColor: mainColor,
                            labelColor: mainColor,
                            unselectedLabelColor: nyeusi,



                            tabs: [
                              Tab(child:  Text(
                                "CURRENT SITES",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                ),

                              ),
                              ),

                            ],
                          ),
                        ),
                      ),
                      Container(
                        //Add this to give height
                        height: MediaQuery.of(context).size.height,
                        child: TabBarView(children: [
                          Container(
                            color: mainColor,
                            child: FutureBuilder(
                              future: _getUserss(),
                              builder: (BuildContext context, AsyncSnapshot snapshot){
                                print(snapshot.data);
                                if(snapshot.data == null){
                                  return Container(
                                      color:listcolorr,
                                      child: Center(
                                          child: CircularProgressIndicator()
                                      )
                                  );
                                } else {
                                  return
                                    Container(
                                      color:listcolorr,
                                      child: ListView.builder(
                                        padding: EdgeInsets.only(bottom:300, top:5),
                                        itemCount: snapshot.data.length,
                                        itemBuilder: (BuildContext context, int index) {
                                          return Container(
                                            color: mainColor,
                                            child: Card(
                                              color: tabcolor,
                                              margin: EdgeInsets.all(0.5),
                                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                                              elevation: 4.0,
                                              child: Container(
                                                padding: EdgeInsets.only(right:12.5),

                                                decoration: BoxDecoration(color: listcolorr,
                                                  borderRadius: BorderRadius.circular(10),
                                                ),
                                                child: ExpansionTile(
                                                    title: Container(
                                                      width: double.infinity,


                                                      child: Column(
                                                        crossAxisAlignment: CrossAxisAlignment.start,
                                                        children: <Widget>[
                                                          Container(height: 3, color: Colors.transparent),
                                                          Text("Task: "+snapshot.data[index].TaskId+"/"+snapshot.data[index].TaskNumber,style: TextStyle( fontSize: 18, color:nyeusi,
                                                              fontFamily: 'Montserrat-Regular',
                                                              decoration: TextDecoration.underline,
                                                              fontWeight: FontWeight.bold),),
                                                          Container(height: 5, color: Colors.transparent),
                                                          Text("Description: "+snapshot.data[index].TaskTitle,style: TextStyle(fontSize: 16),),
                                                          Container(height: 5, color: Colors.transparent),

                                                          Text.rich(
                                                            TextSpan(
                                                              children: [
                                                                TextSpan(text: "Task Status: ",
                                                                  style: TextStyle(
                                                                    fontSize: 16,
                                                                    color:nyeusi,
                                                                    fontFamily: 'Montserrat-Regular',


                                                                  ),),
                                                                TextSpan(
                                                                  text: snapshot.data[index].Status,

                                                                  style: TextStyle(
                                                                    fontSize: 16,
                                                                    color:nyeusi,
                                                                    fontFamily: 'Montserrat-Regular',),
                                                                ),
                                                              ],
                                                            ),
                                                          ),

                                                          Container(height: 8, color: Colors.transparent),
                                                        ],
                                                      ),
                                                    ),
                                                    onExpansionChanged: (value){
                                                      setState(() {
                                                      });
                                                    },
                                                    children: [
                                                      Container(
                                                        padding: EdgeInsets.only(right: 15.0, top: 5, left:15.0),

                                                        child: Column(
                                                          crossAxisAlignment: CrossAxisAlignment.start,
                                                          children: <Widget>[

                                                            Text.rich(
                                                              TextSpan(
                                                                children: [
                                                                  TextSpan(text: "Project Code: ",
                                                                    style: TextStyle(
                                                                      fontSize: 16,
                                                                      color:nyeusi,
                                                                      fontFamily: 'Montserrat-Regular',


                                                                    ),),
                                                                  TextSpan(
                                                                    text: snapshot.data[index].ProjectCode,

                                                                    style: TextStyle(
                                                                      fontSize: 16,
                                                                      color:nyeusi,
                                                                      fontFamily: 'Montserrat-Regular',),
                                                                  ),
                                                                ],
                                                              ),
                                                            ),

                                                            Container(height: 8, color: Colors.transparent),
                                                            Text.rich(
                                                              TextSpan(
                                                                children: [
                                                                  TextSpan(text: "Project Name: ",
                                                                    style: TextStyle(
                                                                      fontSize: 16,
                                                                      color:nyeusi,
                                                                      fontFamily: 'Montserrat-Regular',


                                                                    ),),
                                                                  TextSpan(
                                                                    text: snapshot.data[index].ProjectName,

                                                                    style: TextStyle(
                                                                      fontSize: 16,
                                                                      color:nyeusi,
                                                                      fontFamily: 'Montserrat-Regular',),
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                            Container(height: 8, color: Colors.transparent),
                                                            Text.rich(
                                                              TextSpan(
                                                                children: [
                                                                  TextSpan(text: "Milestone Name: ",
                                                                    style: TextStyle(
                                                                      fontSize: 16,
                                                                      color:nyeusi,
                                                                      fontFamily: 'Montserrat-Regular',


                                                                    ),),
                                                                  TextSpan(
                                                                    text: snapshot.data[index].MileStoneName,

                                                                    style: TextStyle(
                                                                      fontSize: 16,
                                                                      color:nyeusi,
                                                                      fontFamily: 'Montserrat-Regular',),
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                            Container(height: 8, color: Colors.transparent),



                                                          ],
                                                        ),
                                                      ),
                                                    ]
                                                ),
                                              ),
                                            ),
                                          );
                                        },
                                      ),
                                    );
                                }
                              },
                            ),


                          ),


                        ]),
                      ),
                    ],
                  ),
                ),

              ],
            ),
          ],

        ),
      ),


    );
  }

  _textMe(String number) async {
    // Android
    String uri = "sms:$number";
    if (await canLaunch(uri)) {
      await launch(uri);
    } else {
      // iOS
      String uri = "sms:$number";
      if (await canLaunch(uri)) {
        await launch(uri);
      } else {
        throw 'Could not launch $uri';
      }
    }
  }

  _launchCaller(String number) async {
    String url = "tel:$number";
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

}

class PDFViewerFromUrl extends StatelessWidget {
  const PDFViewerFromUrl({Key key, @required this.url}) : super(key: key);

  final String url;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(pdfurl),
      ),
      body: const PDF().fromUrl(
        url,
        placeholder: (double progress) => Center(child: Text('$progress %')),
        errorWidget: (dynamic error) => Center(child: Text(error.toString())),
      ),
    );
  }
}


class TaskList {
  String TaskId;
  String ProjectCode;
  String ProjectName;
  String MileStoneName;
  String TaskNumber;
  String TaskTitle;
  String Priority;
  String Status;




  TaskList(this.TaskId, this.ProjectCode, this.ProjectName, this.MileStoneName,this.TaskNumber, this.TaskTitle, this.Priority, this.Status);

}
class TaskListt {
  String TaskId;
  String ProjectCode;
  String ProjectName;
  String MileStoneName;
  String TaskNumber;
  String TaskTitle;
  String Priority;
  String Status;


  TaskListt(this.TaskId, this.ProjectCode, this.ProjectName, this.MileStoneName,this.TaskNumber, this.TaskTitle, this.Priority, this.Status);
}

class Holidays {
  String HolidayId;
  String HolidayName;
  String HolidayDescription;
  String HolidayStartDateTime;
  String HolidayEndDateTime;

  Holidays(this.HolidayId, this.HolidayName, this.HolidayDescription, this.HolidayStartDateTime,  this.HolidayEndDateTime);
}
