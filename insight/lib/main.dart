import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'dart:convert' as convert;
import 'package:flutter_session/flutter_session.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:url_launcher/url_launcher.dart';


import 'dart:convert';

import 'ClientDashboard.dart';
import 'dart:io';

import 'Dashboard.dart';


String os = Platform.operatingSystem;
String user, jina, etoken=" ";
ProgressDialog pr;
String role="0";


FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
const mainColorr = Color(0xff089225);
const mainColor = Color(0xff7f5496);
const mainColorrr = Color(0xff625a66);
const nyekundu = Color(0xffe82224);
const tabcolor = Color(0xffe1dede);
const listcolor = Color(0xffffbfbfd);
const kijani = Colors.green;
const gray = Color(0xffa9a9a9);

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  jina = prefs.getString('user_id');
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitDown,
    DeviceOrientation.portraitUp,
  ]);
  WidgetsFlutterBinding.ensureInitialized();
  await SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  OneSignal.shared.setLogLevel(OSLogLevel.verbose, OSLogLevel.none);

  OneSignal.shared.init(
      "d64f6095-b8ff-41bc-aaf9-d9d4e6f7e5d0",

      iOSSettings: {
        OSiOSSettings.autoPrompt: false,
        OSiOSSettings.inAppLaunchUrl: false
      }
  );
  OneSignal.shared.setInFocusDisplayType(OSNotificationDisplayType.notification);

  runApp(MyApp());

}

class MyApp extends StatelessWidget {
  Future<bool> isLoggedIn() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    jina = prefs.getString('name');
    //role= prefs.getString('isSFP');
    print(jina);
    if(jina!=null)
      return false;
    else
      return false;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: FutureBuilder(
          future: isLoggedIn(),
          builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
            if (snapshot.hasData) {
              return snapshot.data ? (Dashboard()) : LoginPage() ;
            }
            return Container(); // noop, this builder is called again when the future completes
          },
        )
    );
  }
}

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();

}

class _LoginPageState extends State<LoginPage> {
  bool _obscureText = true;
  String email, password;
  String username;
  String userId;
  int companyId; 
  String companyName;
  List<Null> emergencyContacts;
  String latestMessage;
  String iconUrl;
  bool isAuthenticated;
  String authMessage;
  final _text = TextEditingController();
  bool _validate = false;
  final _formKey = GlobalKey<FormState>();
  final _formKeyy = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    firebaseCloudMessaging_Listeners();

  }


  Future<void> _doSignIn() async {
    print(email);
    print(password);
    print(os);
    print(etoken);
    pr.show();
    String apiUrl = "http://projects.qooetu.com/mobile/V2/userlogin";
    Map<String, String> headers = {"Content-type": "application/json"};
    final json =  convert.jsonEncode({"email": email, "password": password,"DeviceType": os,"DeviceToken": etoken});
    http.Response response = await http.post(apiUrl,headers: headers, body: json);
    String jsonsDataString = response.body.toString();
    print("oromo"+jsonsDataString);
    if (response.statusCode == 200) {
      pr.hide().then((isHidden) {
        print(isHidden);
      });





      Map<String, dynamic> json = jsonDecode(jsonsDataString);

      Map<String, dynamic> map=(json['basic_details']);

      if(map==null)
      {
        Fluttertoast.showToast(
            msg: "Incorrect Username Or Password",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 3,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );
      }


      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString("user_id", map['user_id'].toString());
      prefs.setString("token",map['token'].toString());
      prefs.setString("name", map['name'].toString());
      prefs.setString("telephone", map['telephone'].toString());
      prefs.setString("avatar", map['avatar'].toString());
      prefs.setString("email", map['email'].toString());
      prefs.setString("IdNno", map['IdNno'].toString());
      prefs.setString("dob", map['dob'].toString());
      prefs.setString("Country", map['Country'].toString());
      prefs.setString("StaffNumber", map['StaffNumber'].toString());
      prefs.setString("Department", map['Department'].toString());
      prefs.setString("Sex", map['Sex'].toString());
      prefs.setString("JobTitle", map['JobTitle'].toString());
      prefs.setString("timezone", map['timezone'].toString());
      prefs.setString("CompanyName", map['CompanyName'].toString());
      prefs.setString("CompnayTelephone", map['CompnayTelephone'].toString());
      prefs.setString("CompanyLogo", map['CompanyLogo'].toString());



      Navigator.push(context, new MaterialPageRoute(
          builder: (context) => new ClientDashboard())
      );
    }
    else {
      pr.hide().then((isHidden) {
        print(isHidden);
      });
      Fluttertoast.showToast(
          msg: "Incorrect Username Or Password",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 3,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0
      );
      print("nooo");
    }
  }


  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context,type: ProgressDialogType.Normal, isDismissible: true, showLogs: true);
    pr.style(
        message: 'Logging in...',
        borderRadius: 10.0,
        backgroundColor: Colors.grey.withOpacity(0.8),
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600)
    );
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
          systemNavigationBarColor: mainColor,
          statusBarColor: mainColor),
    );
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  color: mainColor,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(80),
                  ),
                ),
                height: 210,
                child: Padding(
                  padding: EdgeInsets.only(top: 20),
                  child: Column(
                    children: <Widget>[
                      Center(
                        child: Container(
                          height: 100.0,
                          width:  MediaQuery.of(context).size.width ,
                          child: new Image.asset('assets/gi.png'),


                        ),
                      ),
                      Center(
                        child: Center(
                          child:               Container(
                            width: 120.0,
                            height:40,
                            child: FittedBox(
                                fit: BoxFit.contain,
                                child: RichText(
                                  textAlign: TextAlign.center,
                                  text: TextSpan(children: <TextSpan>[

                                    TextSpan(
                                        text: "PMS",
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontFamily: 'Microsoft Himalaya',
                                        )),

                                  ]),
                                )

                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 20, right: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Text(
                              'Login',
                              style: TextStyle(color: Colors.white),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 100,
              ),

              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(40),
                    ),
                  ),
                  child: TextFormField(
                    onChanged: (value) {
                      setState(() {
                        email = value;
                      });
                    },
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      prefixIcon: Icon(Icons.mail),
                      hintText: 'Email',
                    ),
                    autocorrect: true,

                    // decoration: TextDecoration(),
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(40),
                    ),
                  ),
                  child: TextFormField(
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      prefixIcon: Icon(Icons.vpn_key),
                      hintText: 'Password',
                    ),
                  obscureText: true,
                  onChanged: (value) {
                  setState(() {
                  password = value;
                  });},
                    // decoration: TextDecoration(),
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  FlatButton(
                    onPressed: () {},
                    child: Text(
                      "Forgot Password?",
                      style: TextStyle(color: Colors.blue),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 40,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 30, right: 30),
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(40),
                    ),
                  ),
                  onPressed: () {
                    _doSignIn();
                  },
                  color: mainColorrr,
                  child: Center(
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Text(
                        'LOGIN',
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),

            ],
          ),
        ),
      ),
      bottomNavigationBar:  Container(
          padding: EdgeInsets.only(bottom: 10),
          color: Colors.white,
          child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget> [
                new RichText(
                  text: new TextSpan(text: 'I agree to ', style: TextStyle(
                    color: Colors.black,
                  ), children: [
                    TextSpan(
                      text: 'Terms',
                      style: TextStyle(
                        color: mainColorr,
                        decoration: TextDecoration.underline,
                      ),
                      recognizer: new TapGestureRecognizer()..onTap = () => -_launchTerms(),
                    ),
                    TextSpan(
                      text: ' and ',
                      style: TextStyle(
                        color: Colors.black,
                      ),
                    ),
                    TextSpan(
                      text: 'Privacy',
                      style: TextStyle(
                        color: mainColorr,
                        decoration: TextDecoration.underline,

                      ),
                      recognizer: new TapGestureRecognizer()..onTap = () => -_launchPolicy(),
                    ),
                  ]),
                ),
              ]
          )

      ),
    );
  }


  void firebaseCloudMessaging_Listeners() {
    if (Platform.isIOS) iOS_Permission();

    _firebaseMessaging.getToken().then((token){
      etoken=token;
      print("token ni "+token);
      print("os ni "+os);

    });

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print('on message $message');
      },
      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('on launch $message');
      },
    );
  }

  void iOS_Permission() {
    _firebaseMessaging.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true)
    );
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings)
    {
      print("Settings registered: $settings");
    });
  }
  _launchTerms() async {
    const url = 'https://orbit.co.ke/Home/termsofuse';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
  _launchPolicy() async {
    const url = 'https://orbit.co.ke/Home/PrivacyPolicy';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}



